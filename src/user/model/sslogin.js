'use strict';
// +----------------------------------------------------------------------
// | CmPage [ 通用页面框架 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------

/**
 @module admin.model
 */

/**
 * 登录用户的操作类，提供一些操作t_user,vw_user的方法
 * @class admin.model.login
 */
import CMPage from '../../cmpage/model/page.js';

export default class extends CMPage {

    /**
     * 增加某个用户的登录信息
     * @method  addLogin
     * @return {object}  登录记录
     * @param {object} user  登录用户对象
     */
    async addLogin(user,token){
        //产生token值
        token = token || think.uuid();

        //先保存t_login
        //let user = await think.session('user');
        let login ={c_guid:user.c_guid, c_ip:user.ip,c_time:think.datetime(), c_token:token,c_sub_system:user.sub_system,c_memo:user.c_memo};
        //debug(login,'user.addLogin - login');
        await this.model('t_sslogin').where({c_guid:user.c_guid}).delete();
        await this.model('t_sslogin').add(login);

        //再保存t_login_his
        await this.model('t_sslogin_his').add(login);
        return login;
    }

    /**
     * 根据用户登录的token值取用户的登录记录，
     * @method  getLoginByToken
     * @return {object}  登录信息
     * @param {string} token  登录时产生的令牌
     */
    async getLoginByToken(token){
        let login = await this.model('t_sslogin').where(`c_token='${token}'`).find();
        if(!think.isEmpty(login)){
            await this.model('t_sslogin').where(`c_token='${token}'`).update({c_time:think.datetime()});
            return login;
        }
        return {};
    }

    /**
     * 某个用户的退出登录信息
     * @method  addLogin
     * @param {object} user  登录用户对象
     */
    async exitLogin(user){
        //let user = await think.session('user');
        let login =  await this.model('t_sslogin').where({c_user:user.id}).find();
        if(login){
            await this.model('t_sslogin_his').where({c_login:login.id}).update({c_time_last:think.datetime(), c_url_last:user.urlLast });
        }
        await this.model('t_sslogin').where({c_user:user.id}).delete();
    }

}
